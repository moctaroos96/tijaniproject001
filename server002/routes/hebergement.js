const express = require('express');
const hebergement = express.Router();
const cors = require('cors');
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');

const Hebergement = require('../models/hebergement');
hebergement.use(cors());



hebergement.post('/reserver_hebergement', (req, res)=>{
    console.log(req.body);
    const hebergementData = {
        pays : req.body.depart,
        ville : req.body.arrivee,
        date_debut : req.body.date_debut,
        date_fin : req.body.date_fin,
        nombre_de_personne : req.body.nombre_de_personne
    }

    Hebergement.create(hebergementData)
    .then(heber => {
        console.log(heber)
        res.json({success : '1', mes : "Reservation avec succée"})
    }).catch(err=>{
        console.log(err);
        res.json({message : "Erreur de reservation ! Essayer plus tard SVP.."})
    })
})

module.exports = hebergement