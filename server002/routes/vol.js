const express = require('express');
const vols = express.Router();
const cors = require('cors');
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');

const Vol = require('../models/vol');
vols.use(cors());


vols.post('/reserver_vol', (req, res)=>{
    console.log(req.body);
    const volData = {
        depart : req.body.depart,
        arrivee : req.body.arrivee,
        date_debut : req.body.date_debut,
        date_fin : req.body.date_fin,
        nombre_de_personne : req.body.nombre_de_personne
    }

    Vol.create(volData)
    .then(vol => {
        console.log(vol)
        res.json({success : '1', msg : "Reservation avec succée"})
    }).catch(err=>{
        console.log(err);
        res.json({ success : '2' , msg : "Erreur de reservation ! Essayer plus tard SVP.."})
    })
})

module.exports = vols