const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    depart : {
        type: String,
        require : true
    },
    arrivee : {
        type : String,
        require : true
    },
    date_debut : {
        type : String,
        require : true
    },
    date_fin : {
        type : String,
        require : true
    },
    nombre_de_personne : {
        type : Number,
        require : true
    }
});

module.exports = vol = mongoose.model('vol', UserSchema);