const mongoose = require("mongoose");
// const hebergement = require("../routes/hebergement");

const UserSchema = mongoose.Schema({
    pays : {
        type: String,
        require : true
    },
    ville : {
        type : String,
        require : true
    },
    date_debut : {
        type : String,
        require : true
    },
    date_fin : {
        type : String,
        require : true
    },
    nombre_de_personne : {
        type : Number,
        require : true
    }
});

module.exports = hebergement = mongoose.model('hebergement', UserSchema);